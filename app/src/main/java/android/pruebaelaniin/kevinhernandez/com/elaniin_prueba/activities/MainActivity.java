package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Generation;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.adapters.GenerationAdapter;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.GenericClass;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.GlideApp;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.MySingleton;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.R;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Region;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.RegionList;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.User;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    //  View items
    private ImageView mImageViewAvatar, mImageViewMenu;
    private RecyclerView mRecyclerView;
    private TextView mTextViewUser;

    //  Logic items
    private User mLoggedUser;
    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference mDatabasePokemon;
    private List<Region> mRegionsList;
    private List<Generation> mGenerations;
    private static final String SERVER_URL = "https://pokeapi.co/api/v2/";
    private static final String REGION_URL = SERVER_URL + "region/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        //                                  Initializations
        try{
            findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
            Bundle bundle = getIntent().getExtras();
            mLoggedUser = new Gson().fromJson(bundle != null ? bundle.getString("user") : null,User.class);

            mTextViewUser = findViewById(R.id.textViewUser);
            Typeface mCustomFont = Typeface.createFromAsset(getAssets(), "fonts/ostrich-regular.ttf");
            mTextViewUser.setTypeface(mCustomFont);
            mImageViewAvatar = findViewById(R.id.imageViewAvatar);
            mImageViewMenu = findViewById(R.id.imageViewMenu);
            mRecyclerView = findViewById(R.id.recyclerViewGenerations);

            mFirebaseAuth = FirebaseAuth.getInstance();
            mDatabasePokemonSet();
            mRegionsList = new ArrayList<>();
            mGenerations = new ArrayList<>();
            if(mLoggedUser !=null) mImageViewAvatarFill(mLoggedUser);
            mImageViewMenuFill();
            if(mLoggedUser !=null) mTextViewUser.setText(mLoggedUser.getUsername());

            getAllRegions();        }catch (Exception e){
            Log.d("OnCrete", "onCreate: "+e.getMessage());
            Toast.makeText(this, "Error en onCreate: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    //                                  User Created events

    /**
     * Sets the image for the type of user (boy or girl), when the user touches it, it logs out of
     * the app
     * @param loggedUser the user object passed through the bundle parameters
     */
    private void mImageViewAvatarFill(User loggedUser) {
        try{
            int genre = 0;
            switch (loggedUser.getGenre()){
                case "Male":
                    genre = R.drawable.brendan_mini;
                    break;
                case "Female":
                    genre = R.drawable.may_mini;
                    break;
                default:
                    break;
            }
            GlideApp
                    .with(this)
                    .load(genre)
                    //.fitCenter()
                    .centerCrop()
                    .into(mImageViewAvatar);

            mImageViewAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mFirebaseAuth.signOut();
                    Intent intentLogin = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intentLogin);
                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en mImageViewAvatarFill: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * Sets the region table reference from the Firebase DB, and sets the complete list of regions
     * in a List
     */
    private void mDatabasePokemonSet() {
        try{
            mDatabasePokemon = FirebaseDatabase.getInstance().getReference("region");

            mDatabasePokemon.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try{
                        for (DataSnapshot item : dataSnapshot.getChildren()){
                            mRegionsList.add(item.getValue(Region.class));
                        }
                        Collections.sort(mRegionsList, new Comparator<Region>() {
                            @Override
                            public int compare(Region o1, Region o2) {
                                return o1.getId() - o2.getId();
                            }
                        });
                    }catch (Exception e){
                        Toast.makeText(MainActivity.this, "Error en mDatabasePokemonSet.onDataChange: " + e.getClass(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Toast.makeText(MainActivity.this, "Database canceled", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en mDatabasePokemonSet: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Sets the image for the menu ImageView, when the user touches it, it redirects to the Teams
     * module
     */
    private void mImageViewMenuFill() {
        try{
            GlideApp
                    .with(this)
                    .load(R.drawable.menu_icon)
                    .into(mImageViewMenu);

            mImageViewMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentTeams = new Intent(MainActivity.this, TeamsActivity.class);
                    intentTeams.putExtra("user", new Gson().toJson(mLoggedUser));
                    startActivity(intentTeams);
                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en mImageViewMenuFill: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Creates the RecyclerView for the list of regions in the Database, showing the region name
     * and image (mGenerations List must be initialized first)
     */
    private void createRecyclerView() {
        try{
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

            RecyclerView.Adapter mAdapter = new GenerationAdapter(mGenerations, R.layout.recycler_item_card, new GenerationAdapter.onItemClickListener() {
                @Override
                public void onItemClick(Generation generation, int position) {
                    try{
                        Intent intentPokedex = new Intent(MainActivity.this, PokedexActivity.class);
                        intentPokedex.putExtra("region", new Gson().toJson(mRegionsList.get(position)));
                        intentPokedex.putExtra("user", new Gson().toJson(mLoggedUser));
                        startActivity(intentPokedex);
                    }catch (Exception e){
                        Toast.makeText(MainActivity.this, "Error en createRecyclerView.onItemClick: " + e.getClass(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());

            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mAdapter);
        }catch (Exception e){
            Toast.makeText(this, "Error en createRecyclerView: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Gets all the API regions and matches it with the local images for filling the RecyclerView
     * item
     */
    private void getAllRegions() {
        try{
            StringRequest stringRequest = new StringRequest(Request.Method.GET, REGION_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try{
                                RegionList regions = new Gson().fromJson(response, RegionList.class);
                                for (GenericClass gr: regions.getResults()){
                                    int id=0;
                                    switch (gr.getName()){
                                        case "johto":
                                            id = R.drawable.kanto;
                                            break;
                                        case "kanto":
                                            id = R.drawable.johto;
                                            break;
                                        case "hoenn":
                                            id = R.drawable.hoenn;
                                            break;
                                        case "sinnoh":
                                            id = R.drawable.sinnoh;
                                            break;
                                        case "unova":
                                            id = R.drawable.unova;
                                            break;
                                        case "kalos":
                                            id = R.drawable.kalos;
                                            break;
                                        case "alola":
                                            id = R.drawable.alola;
                                            break;
                                        default:
                                            break;
                                    }
                                    mGenerations.add(new Generation(gr.getName(),id));
                                }
                                createRecyclerView();
                                findViewById(R.id.loadingPanel).setVisibility(View.INVISIBLE);
                            }catch (Exception e){
                                Toast.makeText(MainActivity.this, "Erro en getAllRegions.onResponse: " + e.getClass(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(MainActivity.this, "Retrying to connect", Toast.LENGTH_SHORT).show();
                }
            });

            stringRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 5000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 10;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });

            MySingleton.getInstance(MainActivity.this).addToRequestQueue(stringRequest);
        }catch (Exception e){
            Toast.makeText(this, "Error en getAllRegions: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }
}
//  TODO check all for statements