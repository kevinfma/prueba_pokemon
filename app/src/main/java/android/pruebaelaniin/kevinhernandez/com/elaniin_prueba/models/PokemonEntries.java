package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

import java.util.List;

/**
 * Created by kevinFma on 3/26/2018.
 *
 * Class used for getting the pokemon image and name in the TeamsActivity
 */

public class PokemonEntries {
    private int entry_number;
    private PokemonSpecies pokemon_species;

    public PokemonEntries(int entry_number, PokemonSpecies species){
        this.setEntry_number(entry_number);
        this.setPokemon_species(species);
    }

    public PokemonEntries(){}

    public int getEntry_number() {
        return entry_number;
    }

    public void setEntry_number(int entry_number) {
        this.entry_number = entry_number;
    }

    public PokemonSpecies getPokemon_species() {
        return pokemon_species;
    }

    public void setPokemon_species(PokemonSpecies pokemon_species) {
        this.pokemon_species = pokemon_species;
    }
}
