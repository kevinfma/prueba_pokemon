package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.GlideApp;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.MySingleton;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.adapters.PokedexAdapter;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.R;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.adapters.PokemonAdapter;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Pokedex;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.PokemonEntries;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.PokemonSpecies;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Region;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Slots;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Team;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.User;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PokedexActivity extends AppCompatActivity {

    //  View items

    private ImageView mImageViewAvatar, mImageViewMenu;
    private ImageView mPkmn1, mPkmn2, mPkmn3, mPkmn4, mPkmn5, mPkmn6;
    private TextView mTextViewUser, mTextViewSelect;
    private LinearLayout mLayoutPokeballs;
    private RecyclerView mRecyclerViewPokedex, mRecyclerViewPokemon;

    //  Logic items

    private User mLoggedUser;
    private Region mRegion;
    private RecyclerView.Adapter mAdapterPokemon;
    private List<String> mPokedexesList;
    private List<PokemonSpecies> mSpeciesList;
    private List<PokemonSpecies> mTeamList;
    private boolean mSelectingTeam = false;
    private DatabaseReference mPokemonDatabase;
    private Team mEditTeam;
    private static final String SERVER_URL = "https://pokeapi.co/api/v2/";
    private static final String SPECIES_URL = SERVER_URL + "pokemon-species/";
    private static final String SERVER_IMAGE_URL = "http://pokeapi.co/media/sprites/pokemon/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokedex);

        //                                  Initializations
        final Bundle bundle = getIntent().getExtras();
        mLoggedUser = new Gson().fromJson(bundle != null ? bundle.getString("user") : null, User.class);
        mRegion = new Gson().fromJson(bundle != null ? bundle.getString("region") : null,Region.class);
        mEditTeam = new Gson().fromJson(bundle != null ? bundle.getString("edit") : null, Team.class);

        Typeface mCustomFont = Typeface.createFromAsset(getAssets(), "fonts/ostrich-regular.ttf");
        mTextViewUser = findViewById(R.id.textViewUser);
        mTextViewUser.setTypeface(mCustomFont);
        mTextViewSelect = findViewById(R.id.textViewSelect);
        mTextViewSelect.setTypeface(mCustomFont);
        mLayoutPokeballs = findViewById(R.id.layoutPokeballs);
        mRecyclerViewPokedex = findViewById(R.id.recyclerViewPokedex);
        mRecyclerViewPokemon = findViewById(R.id.recyclerViewPokemon);
        mImageViewMenu = findViewById(R.id.imageViewMenu);
        mImageViewAvatar = findViewById(R.id.imageViewAvatar);

        mPokemonDatabase = FirebaseDatabase.getInstance().getReference("team");
        mPokedexesList = new ArrayList<>();
        mSpeciesList = new ArrayList<>();
        mTeamList = new ArrayList<>();
        if (mLoggedUser != null){
            mTextViewUser.setText(mLoggedUser.getUsername());
            mImageViewAvatarFill(mLoggedUser);
        }
        pkmnSet();
        if (mEditTeam == null){
            createMenuImage();
        }else{
            createEditUI();
        }
        createRecyclerView();

    }//     --OnCreate

    //                                  User created events

    /**
     * Sets the click event for each pokeball ImageView when the user selects create team, when
     * any gets touched, removes the selected pokemon from the team slot (if it has one)
     */
    private void pkmnSet() {
        try{
            mPkmn1 = findViewById(R.id.pokeball1);
            mPkmn2 = findViewById(R.id.pokeball2);
            mPkmn3 = findViewById(R.id.pokeball3);
            mPkmn4 = findViewById(R.id.pokeball4);
            mPkmn5 = findViewById(R.id.pokeball5);
            mPkmn6 = findViewById(R.id.pokeball6);

            mPkmn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removePokemon(0);
                }
            });
            mPkmn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removePokemon(1);
                }
            });
            mPkmn3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removePokemon(2);
                }
            });
            mPkmn4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removePokemon(3);
                }
            });
            mPkmn5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removePokemon(4);
                }
            });
            mPkmn6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removePokemon(5);
                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en pkmnSet: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Sets the image for the type of user (boy or girl), when the user touches it, it logs out of
     * the app
     * @param loggedUser the user object passed through the bundle parameters
     */
    private void mImageViewAvatarFill(User loggedUser) {
        try{
            int genre = 0;
            switch (loggedUser.getGenre()){
                case "Male":
                    genre = R.drawable.brendan_mini;
                    break;
                case "Female":
                    genre = R.drawable.may_mini;
                    break;
                default:
                    break;
            }
            GlideApp
                    .with(this)
                    .load(genre)
                    //.fitCenter()
                    .centerCrop()
                    .into(mImageViewAvatar);
        }catch (Exception e){
            Toast.makeText(this, "Erro en mImageViewAvatarFill: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Creates the UI for the edit team process, this function is executed then the "edit" parameter
     * in bundle is not null (when the user touches the edit button in the Teams module)
     */
    private void createEditUI() {
        try{
            hideUserData();
            final int size = mEditTeam.getSlots().size();
            final List<Slots> slotsList = mEditTeam.getSlots();
            for (int i = 0; i < size; i++){
                mTeamList.add(new PokemonSpecies(String.valueOf(slotsList.get(i).getPokemonNumber()),slotsList.get(i).getName()));
            }
            orderPokemon();

            mImageViewMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{
                        if (mTeamList.size() < 3){
                            AlertDialog.Builder builder = new AlertDialog.Builder(PokedexActivity.this);
                            builder.setTitle("You have to select at least 3 pokemons, do you wish to cancel selection?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    try{
                                        Intent intentTeams = new Intent(PokedexActivity.this, TeamsActivity.class);
                                        intentTeams.putExtra("user", new Gson().toJson(mLoggedUser));
                                        startActivity(intentTeams);
                                    }catch (Exception e){
                                        Toast.makeText(PokedexActivity.this, "Error en createEditUI.onClick.dialogInterface: " + e.getClass(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                            builder.setNegativeButton("Continue selecting", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                            builder.show();
                        }else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(PokedexActivity.this);
                            builder.setTitle("Have you finished updating your team?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    try{
                                        insertEditTeam(mEditTeam.getName());
                                        Intent intentTeams = new Intent(PokedexActivity.this, TeamsActivity.class);
                                        intentTeams.putExtra("user", new Gson().toJson(mLoggedUser));
                                        startActivity(intentTeams);
                                    }catch (Exception e){
                                        Toast.makeText(PokedexActivity.this, "Error en createEditUI.onClick.dialogInterface: " + e.getClass(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                            builder.setNegativeButton("Continue selecting", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                            builder.show();
                        }
                    }catch (Exception e){
                        Toast.makeText(PokedexActivity.this, "Error en createEditUI.onClick: " + e.getClass(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en createEditUI: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Creates the menu image at the top right part of window, when it gets touched starts the
     * create team process, showing 6 pokeballs instead of the user data. When the user finishes
     * creating the team and touches this image, it ask for selecting a name and saves the team
     */
    private void createMenuImage() {
        try{
            GlideApp
                    .with(this)
                    .load(R.drawable.pokeballs)
                    .into(mImageViewMenu);

            mImageViewMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{
                        if (mSpeciesList.size() == 0){
                            Toast.makeText(PokedexActivity.this, "You haven't selected a pokedex", Toast.LENGTH_SHORT).show();
                        }else{
                            if (mSelectingTeam){
                                if (mTeamList.size() < 3){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(PokedexActivity.this);
                                    builder.setTitle("You have to select at least 3 pokemons, do you wish to cancel selection?");
                                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            try{
                                                mTeamList.clear();
                                                orderPokemon();
                                                mImageViewAvatarFill(mLoggedUser);
                                                if(mLoggedUser !=null) mTextViewUser.setText(mLoggedUser.getUsername());
                                                showUserData();
                                            }catch (Exception e){
                                                Toast.makeText(PokedexActivity.this,
                                                        "Error en createMenuImage.new onClicklistener.onClick.new onClicklistener.onClick: " + e.getClass(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                    builder.setNegativeButton("Continue selecting", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    });
                                    builder.show();
                                }else{
                                    AlertDialog.Builder builder = new AlertDialog.Builder(PokedexActivity.this);
                                    builder.setTitle("Insert a name for your team!");
                                    final EditText input = new EditText(PokedexActivity.this);
                                    builder.setView(input);
                                    builder.setPositiveButton("Finish", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            insertEditTeam(input.getText().toString().trim());
                                        }
                                    });
                                    builder.setNegativeButton("No, continue selecting", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    });
                                    builder.show();
                                }
                            }else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(PokedexActivity.this);
                                builder.setTitle("Do you wan to select a new eam?");
                                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        hideUserData();
                                    }
                                });
                                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });
                                builder.show();
                            }
                        }
                    }catch (Exception e){
                        Toast.makeText(PokedexActivity.this, "Error en createMenuImage.new onClicklistener.onClick: " + e.getClass(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en createMenuImage: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Inserts a new team or edits one in the Database
     * @param name the team name
     */
    private void insertEditTeam(String name) {
        try{
            List<Slots> slots = new ArrayList<>();
            for (PokemonSpecies item : mTeamList){
                slots.add(new Slots(item.getName(), SERVER_IMAGE_URL + item.getUrl() + ".png",Integer.parseInt(item.getUrl())));
            }
            String idTeam, code;
            boolean isPublic;
            if (mEditTeam == null){

                idTeam = mPokemonDatabase.push().getKey();
                code = String.format("%04d",new Random().nextInt(10000));
                isPublic = false;
            }else{
                idTeam = mEditTeam.getId();
                code = mEditTeam.getCode();
                isPublic = mEditTeam.isPublic();
            }
            Team newTeam = new Team(
                    idTeam,
                    name,
                    mRegion.getId(),
                    mLoggedUser.getId(),
                    mTeamList.size(),
                    slots,
                    isPublic,
                    code
            );
            mPokemonDatabase.child(idTeam).setValue(newTeam).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    try{
                        if (task.isSuccessful()){
                            Toast.makeText(PokedexActivity.this, "Team created successfully", Toast.LENGTH_SHORT).show();
                            Intent intentMain = new Intent(PokedexActivity.this, MainActivity.class);
                            intentMain.putExtra("user", new Gson().toJson(mLoggedUser));
                            startActivity(intentMain);
                        }
                    }catch (Exception e){
                        Toast.makeText(PokedexActivity.this, "Error en insertEditTeam.new OnCompleteListener.onComplete: " + e.getClass(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en insertEditTeam: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Creates the RecyclerView objects for the pokedex and pokemon list
     */
    private void createRecyclerView() {
        try{
            mRecyclerViewPokedex.setHasFixedSize(true);
            mRecyclerViewPokemon.setHasFixedSize(true);

            RecyclerView.LayoutManager layoutManagerPokedex = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            RecyclerView.LayoutManager layoutManagerPokemon = new GridLayoutManager(this, 3);

            mRecyclerViewPokedex.setLayoutManager(layoutManagerPokedex);
            mRecyclerViewPokemon.setLayoutManager(layoutManagerPokemon);

            if (mRegion.getPokedexes() != null){
                final int size = mRegion.getPokedexes().size();
                for (int i = 0; i < size; i++){
                    mPokedexesList.add(mRegion.getPokedexes().get(i).getName());
                }
            }

            RecyclerView.Adapter mAdapterPokedex = new PokedexAdapter(mPokedexesList, R.layout.recycler_item_pokedex, new PokedexAdapter.onItemClickListener() {
                @Override
                public void onItemClick(String name, int position, View view) {
                    try{
                        clearBackground();
                        view.setBackground(ContextCompat.getDrawable(PokedexActivity.this,R.drawable.border_1));
                        showProgressBar();
                        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                                mRegion.getPokedexes().get(position).getUrl(),
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try{
                                            mSpeciesList.clear();
                                            Pokedex pokedex = new Gson().fromJson(response, Pokedex.class);
                                            for (PokemonEntries entries : pokedex.getPokemon_entries()) {
                                                PokemonSpecies pokemonSpecies = entries.getPokemon_species();
                                                pokemonSpecies.setUrl(pokemonSpecies.getUrl().replace(SPECIES_URL, "").replace("/", ""));
                                                mSpeciesList.add(pokemonSpecies);
                                            }
                                            hideProgressBar();
                                            mAdapterPokemon.notifyDataSetChanged();
                                        }catch (Exception e){
                                            Toast.makeText(PokedexActivity.this, "Error en createRecyclerView.new onItemClickListener.onItemClick.new Listener.onResponse: " + e.getClass(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("VOLLEY", "onErrorResponse: "+error.getMessage());
                                Toast.makeText(PokedexActivity.this, "Error: No se pudo cargar el pokedex " + error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                        stringRequest.setRetryPolicy(new RetryPolicy() {
                            @Override
                            public int getCurrentTimeout() { return 10000; }

                            @Override
                            public int getCurrentRetryCount() { return 10; }

                            @Override
                            public void retry(VolleyError error) throws VolleyError {
                                Toast.makeText(PokedexActivity.this, "Retrying to connect", Toast.LENGTH_SHORT).show();
                            }
                        });
                        MySingleton.getInstance(PokedexActivity.this).addToRequestQueue(stringRequest);
                    }catch (Exception e){
                        Toast.makeText(PokedexActivity.this, "Error en createRecyclerView.new onItemClickListener.onItemClick: " + e.getClass(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mAdapterPokemon = new PokemonAdapter(mSpeciesList, R.layout.recycler_item_pokemon, new PokemonAdapter.onItemClickListener() {
                @Override
                public void onItemClick(final PokemonSpecies species, int position) {
                    try{
                        if (!mSelectingTeam){
                            Toast.makeText(PokedexActivity.this, species.getName(), Toast.LENGTH_SHORT).show();
                        }else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(PokedexActivity.this);
                            builder.setTitle("Do you wish to add " + species.getName() + " to the team?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    addPokemon(species);
                                }
                            });
                            builder.setNegativeButton("No, continue selecting", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                            builder.show();
                        }
                    }catch (Exception e){
                        Toast.makeText(PokedexActivity.this, "Error en createRecyclerView.new onItemClickListener.onItemClick: ", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mRecyclerViewPokedex.setAdapter(mAdapterPokedex);
            mRecyclerViewPokemon.setAdapter(mAdapterPokemon);
        }catch (Exception e){
            Toast.makeText(this, "Error en createRecyclerView: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Clears all the pokedexes borders
     */
    private void clearBackground() {
        try {
            final int size = mRecyclerViewPokedex.getChildCount();
            for (int i = 0; i < size; i++){
                mRecyclerViewPokedex.getChildAt(i).setBackgroundResource(0);
            }
        }catch (Exception e){
            Toast.makeText(this, "Error en clearBackground: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Adds a new pokemon to the team list and shows it int he top left part of window
     * @param species then PokemonSpecies object, contains the pokemon info
     */
    private void addPokemon(PokemonSpecies species) {
        try{
            if (mTeamList.size() < 6){
                mTeamList.add(species);
            }else{
                Toast.makeText(this, "You have reached 6 pokemon!", Toast.LENGTH_SHORT).show();
            }
            orderPokemon();
        }catch (Exception e){
            Toast.makeText(this, "Error en addPokemon: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Removes a pokemon from the selected position in the team list and removes it from the
     * top left part of window
     * @param position
     */
    private void removePokemon(final int position){
        try {
            if (mTeamList.get(position) != null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PokedexActivity.this);
                builder.setTitle("Do you wish to remove " + mTeamList.get(position).getName() + " from the team?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) throws IndexOutOfBoundsException{
                        mTeamList.remove(position);
                        orderPokemon();
                    }
                });
                builder.setNegativeButton("No, keep it here!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.show();
            }
        }catch (IndexOutOfBoundsException iob){
            Log.d("PKMN", "Pokemon not found");
        }catch (Exception e){
            Toast.makeText(this, "Error en removePokeon: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Reorders the pokemon team list in the window
     */
    private void orderPokemon() {
        try{
            GlideApp
                    .with(PokedexActivity.this)
                    .load(R.drawable.pokeball)
                    .into(mPkmn1);
            GlideApp
                    .with(PokedexActivity.this)
                    .load(R.drawable.pokeball)
                    .into(mPkmn2);
            GlideApp
                    .with(PokedexActivity.this)
                    .load(R.drawable.pokeball)
                    .into(mPkmn3);
            GlideApp
                    .with(PokedexActivity.this)
                    .load(R.drawable.pokeball)
                    .into(mPkmn4);
            GlideApp
                    .with(PokedexActivity.this)
                    .load(R.drawable.pokeball)
                    .into(mPkmn5);
            GlideApp
                    .with(PokedexActivity.this)
                    .load(R.drawable.pokeball)
                    .into(mPkmn6);
            switch (mTeamList.size()){
                case 6:
                    GlideApp
                            .with(PokedexActivity.this)
                            .load(SERVER_IMAGE_URL + mTeamList.get(5).getUrl() + ".png")
                            .into(mPkmn6);
                case 5:
                    GlideApp
                            .with(PokedexActivity.this)
                            .load(SERVER_IMAGE_URL + mTeamList.get(4).getUrl() + ".png")
                            .centerCrop()
                            .into(mPkmn5);
                case 4:
                    GlideApp
                            .with(PokedexActivity.this)
                            .load(SERVER_IMAGE_URL + mTeamList.get(3).getUrl() + ".png")
                            .centerCrop()
                            .into(mPkmn4);
                case 3:
                    GlideApp
                            .with(PokedexActivity.this)
                            .load(SERVER_IMAGE_URL + mTeamList.get(2).getUrl() + ".png")
                            .centerCrop()
                            .into(mPkmn3);
                case 2:
                    GlideApp
                            .with(PokedexActivity.this)
                            .load(SERVER_IMAGE_URL + mTeamList.get(1).getUrl() + ".png")
                            .centerCrop()
                            .into(mPkmn2);
                case 1:
                    GlideApp
                            .with(PokedexActivity.this)
                            .load(SERVER_IMAGE_URL + mTeamList.get(0).getUrl() + ".png")
                            .fitCenter()
                            .into(mPkmn1);
                    break;
                default:
                    break;
            }
        }catch (Exception e){
            Toast.makeText(this, "Error en orderPokemon: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Hides the user image and username from window, changes the menu icon image and sets the
     * selecting team state to true
     */
    private void hideUserData() {
        try{
            mImageViewAvatar.setVisibility(View.INVISIBLE);
            mTextViewUser.setVisibility(View.INVISIBLE);
            GlideApp
                    .with(this)
                    .load(R.drawable.check)
                    .into(mImageViewMenu);
            mLayoutPokeballs.setVisibility(View.VISIBLE);
            mSelectingTeam = true;
        }catch (Exception e){
            Toast.makeText(this, "Error en hideUserData: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Shows the user image and username on window, changes the menu icon image and sets the
     * selecting team state to false
     */
    private void showUserData(){
        try{
            mImageViewAvatar.setVisibility(View.VISIBLE);
            mTextViewUser.setVisibility(View.VISIBLE);
            GlideApp
                    .with(this)
                    .load(R.drawable.pokeballs)
                    .into(mImageViewMenu);
            mLayoutPokeballs.setVisibility(View.INVISIBLE);
            mSelectingTeam = false;
        }catch (Exception e){
            Toast.makeText(this, "Error en showUserData: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Shows the progress loading icon at the center of screen
     */
    private void showProgressBar(){
        mRecyclerViewPokedex.setEnabled(false);
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
    }

    /**
     * Hides the progress loading icon at the center of screen
     */
    private void hideProgressBar() {
        mRecyclerViewPokedex.setEnabled(true);
        findViewById(R.id.loadingPanel).setVisibility(View.INVISIBLE);
    }
}