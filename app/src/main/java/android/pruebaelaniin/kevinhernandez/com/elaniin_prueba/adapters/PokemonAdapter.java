package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.adapters;

import android.content.Context;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.GlideApp;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.R;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.PokemonSpecies;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by kevinFma on 3/26/2018.
 *
 * Adapter user for loading all the pokemons from a selected pokedex, this is implemented as a
 * RecyclerView adapter and showed in the create team window
 */

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.ViewHolder> {

    private List<PokemonSpecies> pokemonSpecies;
    private int layout;
    private onItemClickListener itemClickListener;
    private Context context;
    private static final String server_url = "http://pokeapi.co/media/sprites/pokemon/";

    public PokemonAdapter(List<PokemonSpecies> species, int layout, onItemClickListener listener){
        this.pokemonSpecies = species;
        this.layout = layout;
        this.itemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(pokemonSpecies.get(position), itemClickListener);
    }

    @Override
    public int getItemCount() { return pokemonSpecies.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageViewPokemon;
        private TextView textViewPokemon;

        public ViewHolder(View itemView) {
            super(itemView);
            imageViewPokemon = itemView.findViewById(R.id.imageViewPokemon);
            textViewPokemon = itemView.findViewById(R.id.textViewPokemon);
        }

        public void bind(final PokemonSpecies species, final onItemClickListener listener){
            textViewPokemon.setText(species.getName());
            GlideApp
                    .with(itemView.getContext())
                    .load(server_url + species.getUrl() + ".png")
                    .into(imageViewPokemon);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(species, getAdapterPosition());
                }
            });
        }
    }

    public interface onItemClickListener{
        void onItemClick(PokemonSpecies species, int position);
    }
}
