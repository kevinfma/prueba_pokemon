package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

import java.util.List;

/**
 * Created by kevinFma on 3/26/2018.
 *
 * Class used for requesting the pokemon species in the TeamActivity
 */

public class Species {
    private String name;
    private List<FlavorTextEntries> flavor_text_entries;

    public Species(String name, List<FlavorTextEntries> flavor_text_entries){
        this.setFlavor_text_entries(flavor_text_entries);
        this.setName(name);
    }

    public Species(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FlavorTextEntries> getFlavor_text_entries() {
        return flavor_text_entries;
    }

    public void setFlavor_text_entries(List<FlavorTextEntries> flavor_text_entries) {
        this.flavor_text_entries = flavor_text_entries;
    }
}
