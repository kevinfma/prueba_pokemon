package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

/**
 * Created by kevinFma on 3/26/2018.
 *
 * This class represents the pokemon type from the Pokemon API
 */

public class Type {
    private String name;
    private String url;

    public Type(String name, String url){
        this.setName(name);
        this.setUrl(url);
    }

    public Type(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
