package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.BuildConfig;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.R;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.User;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

    //  View items
    private EditText mEditTextEmail, mEditTextPass;
    private Button mButtonLogin;
    private TextView mTextViewRegister, mTextViewLogin;
    private LoginButton mButtonFacebook;
    private SignInButton mSignInButton;

    //  Logic items
    private FirebaseAuth mFirebaseAuth;
    private CallbackManager mCallbackManager;
    private ArrayList<User> mUsers;
    private GoogleSignInOptions mSignInOptions;
    private GoogleApiClient mGoogleApiClient;
    private DatabaseReference mDatabasePokemon;
    private boolean mReadDB = false;
    private static final int REQ_CODE = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            //  Setting facebook configurations
            FacebookSdk.sdkInitialize(getApplicationContext());
            if (BuildConfig.DEBUG) {
                FacebookSdk.setIsDebugEnabled(true);
                FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
            }

            //  Setting google configurations
            mSignInOptions = new GoogleSignInOptions
                    .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail().build();
        }catch (Exception e){
            Toast.makeText(this, "Error: " + e.getClass(), Toast.LENGTH_LONG).show();
        }
        setContentView(R.layout.activity_login);

        //                          Initializations

        try {
            mEditTextEmail = findViewById(R.id.editTextEmail);
            mEditTextPass = findViewById(R.id.editTextPass);

            mFirebaseAuth = FirebaseAuth.getInstance();
            mCallbackManager = CallbackManager.Factory.create();
            mDatabasePokemonSet();
            mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, mSignInOptions).build();
            mUsers = new ArrayList<>();

            mTextViewLoginSet();
            mButtonLoginSet();
            mTextViewRegisterSet();
            mButtonFacebookSet();
            mSignInButtonSet();
        }catch (Exception e){
            Toast.makeText(this, "Error en OnCreate: " + e.getClass(), Toast.LENGTH_LONG).show();
        }
    }//     --OnCreate


    //                              User created events

    /**
     * Sets the font fot the user login TextView
     */
    private void mTextViewLoginSet() {
        try{
            Typeface mCustomFont = Typeface.createFromAsset(getAssets(), "fonts/ostrich-regular.ttf");
            mTextViewLogin = findViewById(R.id.textViewLogin);
            mTextViewLogin.setTypeface(mCustomFont);
        }catch (NullPointerException npe){
            Toast.makeText(this, "Font asset file not found", Toast.LENGTH_LONG).show();
        }catch (Exception e){
            Toast.makeText(this, "Error en mTextViewLoginSet: " + e.getClass(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Sets the Google button for logging in, if the device has a Google session, the app
     * redirects to the Main activity, if not then shows an intent asking for logging in with Google
     */
    private void mSignInButtonSet() {
        try{
            mSignInButton = findViewById(R.id.buttonGoogle);
            mSignInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{
                        Intent intentLogin = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                        startActivityForResult(intentLogin, REQ_CODE);
                    }catch (Exception e){
                        Toast.makeText(LoginActivity.this, "Error en mSignInButtonSet.onclick: " + e.getClass(), Toast.LENGTH_LONG).show();
                    }
                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en mSignInButtonSet: " + e.getClass(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Gets the user table from the Database from Firebase
     */
    private void mDatabasePokemonSet() {
        try{
            mDatabasePokemon = FirebaseDatabase.getInstance().getReference("user");
            mDatabasePokemon.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try{
                        if (!mReadDB){
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                                mUsers.add(snapshot.getValue(User.class));
                            }
                            mReadDB = true;
                        }
                    }catch (Exception e){
                        Toast.makeText(LoginActivity.this, "Error en mDatabasePokemonSet.onDataChange: " + e.getClass(), Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en databaseSet: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Sets the Button for logging in with credentials, if the user forgot to insert the username
     * or password, the app shows a Toast asking the user to insert it
     */
    private void mButtonLoginSet() {
        try{
            mButtonLogin = findViewById(R.id.buttonLogin);

            mButtonLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{
                        String email = mEditTextEmail.getText().toString().trim();
                        String passw = mEditTextPass.getText().toString().trim();
                        if (TextUtils.isEmpty(email)){
                            Toast.makeText(LoginActivity.this, "Please enter email", Toast.LENGTH_SHORT).show();
                        }else if (TextUtils.isEmpty(passw)){
                            Toast.makeText(LoginActivity.this, "Please enter passworf", Toast.LENGTH_SHORT).show();
                        }else{
                            findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
                            mFirebaseAuth.signInWithEmailAndPassword(email, passw).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    try{
                                        if (task.isSuccessful()){
                                            Intent intentLogin = new Intent(LoginActivity.this, MainActivity.class);
                                            User user = getUser(task.getResult().getUser().getEmail());
                                            intentLogin.putExtra("user", new Gson().toJson(user));
                                            startActivity(intentLogin);
                                        }else{
                                            Toast.makeText(LoginActivity.this, "Mail or password incorrects", Toast.LENGTH_SHORT).show();
                                        }
                                    }catch (Exception e){
                                        Toast.makeText(LoginActivity.this, "Error en mButtonLoginSet.onComplete: " + e.getClass(), Toast.LENGTH_SHORT).show();
                                    }finally {
                                        findViewById(R.id.loadingPanel).setVisibility(View.INVISIBLE);
                                    }
                                }
                            });
                        }
                    }catch (Exception e){
                        Toast.makeText(LoginActivity.this, "Error en mButtonLoginSet.Setonclick: " + e.getClass(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en mButtonLoginSet: " + e.getClass(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Sets the Facebook button for logging in, if the device has a Facebook session, the app
     * redirects to the Main activity, if not then shows an intent asking for logging in with Facebook
     */
    private void mButtonFacebookSet() {
        try{
            mButtonFacebook = findViewById(R.id.buttonFacebook);
            mButtonFacebook.setReadPermissions("email","public_profile");
            mButtonFacebook.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    handleFacebookAccessToken(loginResult.getAccessToken());
                }

                @Override
                public void onCancel() {
                    Toast.makeText(LoginActivity.this, "Action cancelled", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(FacebookException error) {
                    Toast.makeText(LoginActivity.this, "An error has occurred", Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en mButtonFacebookSet: " + e.getClass(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Sets the TextView for sending from the login activity to the Register activity
     */
    private void mTextViewRegisterSet() {
        mTextViewRegister = findViewById(R.id.textViewRegister);
        mTextViewRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    Intent intentRegister = new Intent(LoginActivity.this, RegisterActivity.class);
                    startActivity(intentRegister);
                }catch (Exception e){
                    Toast.makeText(LoginActivity.this, "Error en mTextViewRegisterSet: " + e.getClass(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Returns an User object from the user list
     * @param email the user email
     * @return User object: the user information from Database
     */
    private User getUser(String email) {
        try{
            for (int i = 0; i < mUsers.size(); i++){
                if (email.equals(mUsers.get(i).getEmail())){
                    return mUsers.get(i);
                }
            }
            return null;
        }catch (Exception e){
            Toast.makeText(this, "Error en getUser: " + e.getClass(), Toast.LENGTH_LONG).show();
            return null;
        }
    }

    /**
     * Evaluates when the Gacebook session evaluation ends, if the session is confirmed, logs the
     * user in Firebase and redirects to the Main activity
     * @param token the Facebook access token when the logging process is successful
     */
    private void handleFacebookAccessToken(AccessToken token) {

        try{
            AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
            mFirebaseAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            try{
                                if (task.isSuccessful()){
                                    User user = getUser(task.getResult().getUser().getEmail());
                                    if (user != null){
                                        Intent intentLogin = new Intent(LoginActivity.this, MainActivity.class);
                                        intentLogin.putExtra("user", new Gson().toJson(user));
                                        startActivity(intentLogin);
                                    }else{
                                        Toast.makeText(LoginActivity.this, "User not registered", Toast.LENGTH_LONG).show();
                                        mFirebaseAuth.signOut();
                                    }
                                }else{
                                    Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                                }
                            }catch (Exception e){
                                Toast.makeText(LoginActivity.this, "Error en handleFacebookAccessToken.onComplete: " + e.getClass(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }catch (Exception e){
            Toast.makeText(this, "Error en handleFacebookAccessToken: " + e.getClass(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Evaluates when the Google session evaluation ends, if the session is confirmed, logs the
     * user in Firebase and redirects to the Main activity
     * @param result the Google result when the logging process is successful
     */
    private void handleResult(GoogleSignInResult result) {
        try{
            if (result.isSuccess()){
                GoogleSignInAccount account = result.getSignInAccount();
                Intent intentLogin = new Intent(LoginActivity.this, MainActivity.class);
                User user = getUser(account.getEmail());
                if (user != null){
                    intentLogin.putExtra("user", new Gson().toJson(user));
                    startActivity(intentLogin);
                }else{
                    Toast.makeText(this, "User not registered", Toast.LENGTH_SHORT).show();
                }
            }
        }catch (Exception e){
            Toast.makeText(this, "Error en handeResult: " + e.getClass(), Toast.LENGTH_LONG).show();
        }
    }

    //                              Activity events

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            mCallbackManager.onActivityResult(requestCode, resultCode, data);

            if (requestCode==REQ_CODE){
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleResult(result);
            }
        }catch (Exception e){
            Toast.makeText(this, "Error en onActivityresult: " + e.getClass(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try{
            final FirebaseUser currentUser = mFirebaseAuth.getCurrentUser();
            if (currentUser != null){
                User user = getUser(currentUser.getEmail());
                if (user!= null){
                    Intent intentMain = new Intent(LoginActivity.this, MainActivity.class);
                    intentMain.putExtra("user", new Gson().toJson(user));
                    startActivity(intentMain);
                }
            }
        }catch (Exception e){
            Toast.makeText(this, "Error en onStart: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Error on signing in", Toast.LENGTH_LONG).show();
    }
}