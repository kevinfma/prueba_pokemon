package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.R;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.User;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

public class GenreActivity extends AppCompatActivity {

    //  View items
    private ImageView mImageViewMale, mImageViewFemale;
    private Bundle mBundleParameters;
    private TextView mTextViewGenreSelect, mTextViewGenre;
    private Button mButtonRegister;

    //  Logic items
    private int mGenre = 0; //   1-> boy : 2->girl
    private String mUsername = "";
    private DatabaseReference mDatabasePokemon;
    private Typeface mCustomFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre);

        //                                  Initializations

        try{
            mDatabasePokemon = FirebaseDatabase.getInstance().getReference("user");

            mCustomFont = Typeface.createFromAsset(getAssets(), "fonts/ostrich-regular.ttf");
            mImageViewMaleSet();
            mImageViewFemaleSet();
            mButtonRegisterSet();
            mTextViewGenreSet();
            mTextViewGenreSelectSet();
            mBundleParameters = getIntent().getExtras();
        }catch (Exception e){
            Toast.makeText(this, "Error en onCreate: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    //                                      User created events

    /**
     * Sets the girl ImageView and its event, when the user touches it, the app shows a red border
     * around it
     */
    private void mImageViewFemaleSet() {
        mImageViewFemale = findViewById(R.id.imageViewMay);

        mImageViewFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setBackground(ContextCompat.getDrawable(GenreActivity.this,R.drawable.border_1));
                mImageViewMale.setBackgroundResource(0);
                mGenre = 2;
            }
        });
    }

    /**
     * Sets the boy ImageView and its event, when the user touches it, the app shows a red border
     * around it
     */
    private void mImageViewMaleSet() {
        mImageViewMale = findViewById(R.id.imageViewBrendan);

        mImageViewMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setBackground(ContextCompat.getDrawable(GenreActivity.this,R.drawable.border_1));
                mImageViewFemale.setBackgroundResource(0);
                mGenre = 1;
            }
        });
    }

    /**
     * Sets the Button for finishing the register process, if the user hasn't selected boy or girl,
     * shows a Toast asking for select the params
     */
    private void mButtonRegisterSet() {
        try{
            mButtonRegister = findViewById(R.id.buttonRegister);
            mButtonRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{
                        if (mGenre == 0){
                            Toast.makeText(GenreActivity.this, "You haven't selected boy or girl", Toast.LENGTH_SHORT).show();
                        }else if (mUsername.equals("")){
                            Toast.makeText(GenreActivity.this, "You haven't inserted a mUsername", Toast.LENGTH_SHORT).show();
                        }else{
                            String id = mDatabasePokemon.push().getKey();
                            User user = new User(
                                    id,
                                    mBundleParameters.getString("uid"),
                                    mBundleParameters.getString("email"),
                                    mUsername,
                                    mBundleParameters.getString("pass"),
                                    mGenre
                            );
                            mDatabasePokemon.child(id).setValue(user);

                            Intent intentMain = new Intent(GenreActivity.this, MainActivity.class);
                            intentMain.putExtra("user", new Gson().toJson(user));
                            startActivity(intentMain);
                        }
                    }catch (Exception e){
                        Toast.makeText(GenreActivity.this, "Error en mButtonRegisterSet.onClick: ", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (Exception e){
            Toast.makeText(this, "Error en mButtonRegisterSet: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Sets the textView for inserting the username, when the user touches it, shows an DialogAlert
     * asking for it
     */
    private void mTextViewGenreSelectSet() {
        mTextViewGenreSelect = findViewById(R.id.textViewGenreSelect);
        mTextViewGenreSelect.setTypeface(mCustomFont);
        mTextViewGenreSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(GenreActivity.this);
                    mBuilder.setTitle("Insert Username");

                    final EditText mInput = new EditText(GenreActivity.this);
                    mBuilder.setView(mInput);

                    mBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mUsername = mInput.getText().toString().trim();
                            mTextViewGenreSelect.setText(mUsername);
                        }
                    });

                    mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

                    mBuilder.show();
                }catch (Exception e){
                    Toast.makeText(GenreActivity.this, "Error en mTextViewGenreSelectSet.onClick: " + e.getClass(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Sets the font fot the message asking if the user is boy or girl
     */
    private void mTextViewGenreSet() {
        try{
            mTextViewGenre = findViewById(R.id.textViewGenre);
            mTextViewGenre.setTypeface(mCustomFont);
        }catch (Exception e){
            Toast.makeText(this, "Error en mTextViewGenreSet: " + e.getClass(), Toast.LENGTH_LONG).show();
        }
    }
}