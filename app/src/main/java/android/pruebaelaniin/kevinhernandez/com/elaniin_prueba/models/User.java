package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

/**
 * Created by kevinFma on 3/25/2018.
 *
 * User class representing the Database user table
 */

public class User {
    private String id;
    private String uid;
    private String email;
    private String username;
    private String password;
    private int total_teams;
    private String genre;

    public User(String id,String uid, String email, String username, String password, int genre){
        this.setId(id);
        this.setUid(uid);
        this.setEmail(email);
        this.setUsername(username);
        this.setPassword(password);
        this.setTotal_teams(0);
        this.setGenre((genre == 1) ? "Male" : "Female");
    }

    public User(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTotal_teams() {
        return total_teams;
    }

    public void setTotal_teams(int total_teams) {
        this.total_teams = total_teams;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
