package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.adapters;

import android.content.Context;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.GlideApp;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.R;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Generation;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by kevinFma on 3/24/2018.
 *
 * Adapter used for loading all the pokemon generations and regions in the MainActivity,
 * this is implemented as a RecyclerView adapter
 */

public class GenerationAdapter extends RecyclerView.Adapter<GenerationAdapter.ViewHolder> {

    private List<Generation> generations;
    private int layout;
    private onItemClickListener itemClickListener;
    private Context context;

    public GenerationAdapter(List<Generation> generations, int layout, onItemClickListener listener){
        this.generations = generations;
        this.layout = layout;
        this.itemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(generations.get(position), itemClickListener);
    }

    @Override
    public int getItemCount() {
        return generations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewName, textViewPokedex;
        public ImageView imageViewPoster;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewTitle);
            textViewPokedex = itemView.findViewById(R.id.textViewPokedex);
            imageViewPoster = itemView.findViewById(R.id.imageViewPoster);
        }

        public void bind(final Generation generation, final onItemClickListener listener){
            textViewName.setText(generation.getName());
            GlideApp
                    .with(itemView.getContext())
                    .load(generation.getPoster())
                    .override(imageViewPoster.getWidth(),imageViewPoster.getHeight())
                    .into(imageViewPoster);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(generation, getAdapterPosition());
                }
            });
        }
    }

    public interface onItemClickListener{
        void onItemClick(Generation generation, int position);
    }
}
