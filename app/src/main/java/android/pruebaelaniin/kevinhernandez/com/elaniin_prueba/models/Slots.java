package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

/**
 * Created by kevinFma on 3/26/2018.
 *
 * Class representing the slots user in every team created
 */

public class Slots {

    private String name;
    private String url;
    private int pokemonNumber;

    public Slots(String name, String url, int pokemonNumber){
        this.setName(name);
        this.setUrl(url);
        this.setPokemonNumber(pokemonNumber);
    }

    public Slots(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getPokemonNumber() {
        return pokemonNumber;
    }

    public void setPokemonNumber(int pokemonNumber) {
        this.pokemonNumber = pokemonNumber;
    }
}
