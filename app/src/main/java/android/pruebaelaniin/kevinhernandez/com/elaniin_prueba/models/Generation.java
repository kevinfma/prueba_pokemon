package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

/**
 * Created by kevinFma on 3/24/2018.
 *
 * Class used for getting all the generations in the MainActivity
 */

public class Generation {

    private String name;
    private int poster;

    public Generation(String name, int poster){
        this.setName(name);
        this.setPoster(poster);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoster() {
        return poster;
    }

    public void setPoster(int poster) {
        this.poster = poster;
    }
}
