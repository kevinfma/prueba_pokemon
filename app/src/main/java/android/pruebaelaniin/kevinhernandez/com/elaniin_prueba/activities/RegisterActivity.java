package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.R;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity {

    //      View items      //

    private TextView mTextViewRegister, mTextViewLogin;
    private EditText mEditTextEmail, mEditTextPass, mEditTextPass2;
    private FirebaseAuth mFirebaseAuth;
    private Button mButtonRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_register);

        //                                  Initializations

        try{
            mEditTextEmail = findViewById(R.id.editTextEmail);
            mEditTextPass = findViewById(R.id.editTextPass);
            mEditTextPass2 = findViewById(R.id.editTextPass2);

            mTextViewLogin();
            mButtonRegister();
            mTextViewRegisterSet();

            mFirebaseAuth = FirebaseAuth.getInstance();
        }catch (Exception e){
            Toast.makeText(this, "Error en onCreate: " + e.getClass(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Set the login TextView link to the Login activity
     */
    private void mTextViewLogin() {
        mTextViewLogin = findViewById(R.id.textViewLogin);

        mTextViewLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    Intent intentLogin = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intentLogin);
                }catch (Exception e){
                    Toast.makeText(RegisterActivity.this, "Error en mTextViewLoginSet.new OnClickListener.onClick: " + e.getClass(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Sets the Button for the register user process
     */
    private void mButtonRegister() {
        mButtonRegister = findViewById(R.id.buttonRegister);

        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    final String email = mEditTextEmail.getText().toString().trim();
                    final String passw = mEditTextPass.getText().toString().trim();
                    String passw2 = mEditTextPass2.getText().toString().trim();
                    if (TextUtils.isEmpty(email)){
                        Toast.makeText(RegisterActivity.this, "Please enter email", Toast.LENGTH_SHORT).show();
                    }else if (TextUtils.isEmpty(passw)){
                        Toast.makeText(RegisterActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
                    }else if (!passw.equals(passw2)){
                        Toast.makeText(RegisterActivity.this, "Passwords not equals", Toast.LENGTH_SHORT).show();
                    }else{
                        mFirebaseAuth.createUserWithEmailAndPassword(email,passw).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                try{
                                    if (task.isSuccessful()){
                                        String uid = task.getResult().getUser().getUid();
                                        Intent intentGenre = new Intent(RegisterActivity.this, GenreActivity.class);
                                        intentGenre.putExtra("uid", uid);
                                        intentGenre.putExtra("email", email);
                                        intentGenre.putExtra("pass", passw);
                                        startActivity(intentGenre);
                                    }else{
                                        Toast.makeText(RegisterActivity.this, "Error en registro", Toast.LENGTH_SHORT).show();
                                    }
                                }catch (Exception e){
                                    Toast.makeText(RegisterActivity.this, "Error en mButtonRegister.new OnClickListener.onClick.new OnCompleteListener.onComplete: " + e.getClass(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }catch (Exception e){
                    Toast.makeText(RegisterActivity.this, "Error en mButtonRegister.new OnClickListener.onClick: " + e.getClass(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Sets the font to the register user TextView
     */
    private void mTextViewRegisterSet() {
        try{
            Typeface mCustomFont = Typeface.createFromAsset(getAssets(), "fonts/ostrich-regular.ttf");
            mTextViewRegister = findViewById(R.id.textViewRegister);
            mTextViewRegister.setTypeface(mCustomFont);
        }catch (NullPointerException npe){
            Toast.makeText(this, "Font asset file not found", Toast.LENGTH_LONG).show();
        }catch (Exception e){
            Toast.makeText(this, "Error en mTextViewLoginSet: " + e.getClass(), Toast.LENGTH_LONG).show();
        }
    }
}