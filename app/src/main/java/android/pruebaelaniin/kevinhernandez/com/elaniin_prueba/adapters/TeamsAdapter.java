package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.adapters;

import android.content.Context;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.GlideApp;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.R;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Slots;
import android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models.Team;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by kevinFma on 3/26/2018.
 *
 * Adapter user for loading all the logged user teams, this is implemented as a RecyclerView
 * adapter and showed in the TeamsActivity window
 */

public class TeamsAdapter extends RecyclerView.Adapter<TeamsAdapter.ViewHolder>{

    private List<Team> teams;
    private int layout;
    private List<onItemClickListener> itemClickListeners;
    private Context context;

    public TeamsAdapter(List<Team> teams, int layout, List<onItemClickListener> listeners){
        this.teams = teams;
        this.layout = layout;
        this.itemClickListeners = listeners;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(teams.get(position), itemClickListeners);
    }

    @Override
    public int getItemCount() { return teams.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView pkmn1, pkmn2, pkmn3, pkmn4, pkmn5, pkmn6;
        private ImageView imageViewEdit, imageViewDelete, imageViewPrivate;
        private TextView textViewName, textViewRegion, textViewCode;

        public ViewHolder(View itemView) {
            super(itemView);
            pkmn1 = itemView.findViewById(R.id.pokeball1);
            pkmn2 = itemView.findViewById(R.id.pokeball2);
            pkmn3 = itemView.findViewById(R.id.pokeball3);
            pkmn4 = itemView.findViewById(R.id.pokeball4);
            pkmn5 = itemView.findViewById(R.id.pokeball5);
            pkmn6 = itemView.findViewById(R.id.pokeball6);
            imageViewEdit = itemView.findViewById(R.id.imageViewEdit);
            imageViewDelete = itemView.findViewById(R.id.imageViewDelete);
            imageViewPrivate = itemView.findViewById(R.id.imageViewPrivate);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewRegion = itemView.findViewById(R.id.textViewRegion);
            textViewCode = itemView.findViewById(R.id.textViewCode);
        }

        public void bind(final Team team, final List<onItemClickListener> listeners){
            textViewName.setText(team.getName());
            textViewCode.setText("Code: " + team.getCode());
            final List<Slots> slots = team.getSlots();

            if (team.isPublic()){
                GlideApp
                        .with(itemView.getContext())
                        .load(R.mipmap.ic_public)
                        .into(imageViewPrivate);
            }else{
                GlideApp
                        .with(itemView.getContext())
                        .load(R.mipmap.ic_private)
                        .into(imageViewPrivate);
            }
            switch (slots.size()){
                case 6:
                    GlideApp
                            .with(itemView.getContext())
                            .load(slots.get(5).getUrl())
                            .into(pkmn6);
                    pkmn6.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listeners.get(5).onItemClick(slots.get(5), getAdapterPosition(), team);
                        }
                    });
                case 5:
                    GlideApp
                            .with(itemView.getContext())
                            .load(slots.get(4).getUrl())
                            .into(pkmn5);
                    pkmn5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listeners.get(4).onItemClick(slots.get(4), getAdapterPosition(), team);
                        }
                    });
                case 4:
                    GlideApp
                            .with(itemView.getContext())
                            .load(slots.get(3).getUrl())
                            .into(pkmn4);
                    pkmn4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listeners.get(3).onItemClick(slots.get(3), getAdapterPosition(), team);
                        }
                    });
                case 3:
                    GlideApp
                            .with(itemView.getContext())
                            .load(slots.get(2).getUrl())
                            .into(pkmn3);
                    pkmn3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listeners.get(2).onItemClick(slots.get(2), getAdapterPosition(), team);
                        }
                    });
                case 2:
                    GlideApp
                            .with(itemView.getContext())
                            .load(slots.get(1).getUrl())
                            .into(pkmn2);
                    pkmn2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listeners.get(1).onItemClick(slots.get(1), getAdapterPosition(), team);
                        }
                    });
                case 1:
                    GlideApp
                            .with(itemView.getContext())
                            .load(slots.get(0).getUrl())
                            .into(pkmn1);
                    pkmn1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listeners.get(0).onItemClick(slots.get(0), getAdapterPosition(), team);
                        }
                    });
                break;
            }

            imageViewDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listeners.get(6).onItemClick(null, getAdapterPosition(), team);
                }
            });

            imageViewPrivate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listeners.get(7).onItemClick(null, getAdapterPosition(), team);
                }
            });

            imageViewEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listeners.get(8).onItemClick(null, getAdapterPosition(), team);
                }
            });
        }
    }

    public interface onItemClickListener{
        void onItemClick(@Nullable Slots slot, int position, Team team);
    }
}
