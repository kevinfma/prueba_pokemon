package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

/**
 * Created by kevinFma on 3/26/2018.
 *
 * Class used for getting the pokemon image in the TeamsActivity
 */

public class PokemonSpecies {
    private String url;
    private String name;

    public PokemonSpecies(String url, String name){
        this.setName(name);
        this.setUrl(url);
    }

    public PokemonSpecies(){}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
