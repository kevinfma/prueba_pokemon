package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

/**
 * Created by kevinFma on 3/26/2018.
 *
 * Class used for making the types request in the TeamActivity
 */

public class Types {
    private int slot;
    private Type type;

    public Types(int slot, Type type){
        this.setSlot(slot);
        this.setType(type);
    }

    public Types(){}

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
