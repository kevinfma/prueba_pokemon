package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

import java.util.List;

/**
 * Created by kevinFma on 3/26/2018.
 *
 * Class used for displaying all the pokedexes that belong to a region in the PokedexActivity
 */

public class Pokedex {
    private String name;
    private List<PokemonEntries> pokemon_entries;

    public Pokedex(String name, List<PokemonEntries> entries){
        this.setName(name);
        this.setPokemon_entries(entries);
    }

    public Pokedex(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PokemonEntries> getPokemon_entries() {
        return pokemon_entries;
    }

    public void setPokemon_entries(List<PokemonEntries> pokemon_entries) {
        this.pokemon_entries = pokemon_entries;
    }
}
