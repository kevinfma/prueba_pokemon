package android.pruebaelaniin.kevinhernandez.com.elaniin_prueba.models;

import java.util.List;

/**
 * Created by kevinFma on 3/26/2018.
 *
 * Class used for displaying all the pokemons in the PokedexActivity
 */

public class Pokemon {
    private String name;
    private List<Types> types;

    public Pokemon(String name, List<Types> types){
        this.setName(name);
        this.setTypes(types);
    }

    public Pokemon(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Types> getTypes() {
        return types;
    }

    public void setTypes(List<Types> types) {
        this.types = types;
    }
}
